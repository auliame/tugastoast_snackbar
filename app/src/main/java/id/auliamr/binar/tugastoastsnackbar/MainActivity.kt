package id.auliamr.binar.tugastoastsnackbar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import id.auliamr.binar.tugastoastsnackbar.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        showToast()
        showSnackBar()
    }

    private fun showToast() {
        binding.btnToast.setOnClickListener {
            val input = binding.inputName.text.toString()
            Toast.makeText(this, "Selamat Datang, $input !", Toast.LENGTH_LONG).show()
        }
    }

    private fun showSnackBar(){
        binding.btnSnackbar.setOnClickListener {
            val snackbar = Snackbar.make(it, "Ini adalah SnackBar.", Snackbar.LENGTH_INDEFINITE)
            snackbar.setAction("Sudah Tahu"){
                val input = binding.inputName.text.toString()
                Toast.makeText(this, "$input keren banget!!", Toast.LENGTH_SHORT).show()
            }
            snackbar.setAction("Dismiss"){
                snackbar.dismiss()
            }
                .show()
        }
    }

}